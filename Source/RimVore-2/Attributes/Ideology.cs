﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RimVore2
{
    /// <summary>
    /// Custom flag to quickly find ideology related things in case I ever want to move them to their own sub mod
    /// </summary>
    public class IdeologyRelated : Attribute
    {
        public IdeologyRelated() { }
    }
}
